import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { createPost } from '../actions/postActions'

export class PostForm extends Component {
  state = {
    title: '',
    body: ''
  }

  onChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  onSubmit = (e) => {
    e.preventDefault()
    const post = {
      title: this.state.title,
      body: this.state.body
    };

    // call Action
    this.props.createPost(post);
  }

  render() {
    return (
      <div className='App'>
        <h1>Add Post</h1>
        <form onSubmit={this.onSubmit}>
          <div>
            <label>Title</label><br />
            <input type="text" name="title" onChange={this.onChange} value={this.state.title} />
          </div>
          <div>
            <label>Body</label><br />
            <textarea type="text" name="body" onChange={this.onChange} value={this.state.body} />
          </div>
          <div>
            <button type="submit">Save</button>
          </div>
        </form>
      </div>
    )
  }
}

PostForm.propTypes = {
  createPost: PropTypes.func.isRequired,
}

export default connect(null, { createPost })(PostForm);
