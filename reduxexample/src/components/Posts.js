import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { fetchPosts } from '../actions/postActions';

export class Posts extends Component {
  // we don't need our component state since it
  // will be handled by redux

  componentDidMount() {
    this.props.fetchPosts();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps !== this.props) {
      prevProps.posts.unshift(this.props.newPost)
      this.setState({ "post": this.props.newPost })
      return (prevProps, prevState)
    }
  }

  render() {
    const postItems = this.props.posts.map(post => (
      <div key={post.id}>
        <h2>{post.title}</h2>
        <p>{post.body}</p>
      </div>
    ))
    return (
      <div className='App'>
        <h1>Posts</h1>
        {postItems}
      </div>
    )
  }
}

// matching proptypes
Posts.propTypes = {
  fetchPosts: PropTypes.func.isRequired,  // fetchPosts is a function
  posts: PropTypes.array.isRequired,  // posts is a array
  // both of these are now properties of Posts component
  newPost: PropTypes.object  // a new object
}

// this maps our redux state to component properties
const mapStateToProps = state => ({
  posts: state.posts.items,
  // state.posts is the postReducer combined reducer in 'reducers/index.js'
  // state.posts.items is reduced from our postReducer
  newPost: state.posts.item
})

export default connect(mapStateToProps, { fetchPosts })(Posts);
