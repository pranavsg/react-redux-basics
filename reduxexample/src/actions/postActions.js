import { FETCH_POSTS, NEW_POST } from './types';

// CLEANER WAY TO WRITE NESTED FUNCTIONS
export const fetchPosts = () => dispatch => {
  fetch('https://jsonplaceholder.typicode.com/posts/')
    .then(response => response.json())
    .then(posts => dispatch({
      type: FETCH_POSTS,
      payload: posts
    }));
}

export const createPost = (postData) => dispatch => {
  fetch('https://jsonplaceholder.typicode.com/posts', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(postData)
  })
    .then(res => res.json())
    .then(post => dispatch({  // this dispatches a new action which will be reduced by new case
      type: NEW_POST,
      payload: post
    }))
}


// each action is a function which is to be exported
// export function fetchPosts() {
//   return function(dispatch) {
//     fetch('https://jsonplaceholder.typicode.com/posts/')
//       .then(response => response.json())
//       .then(posts => dispatch({
//         type: FETCH_POSTS,
//         payload: posts
//       }));
//   }
// }
