import { FETCH_POSTS, NEW_POST } from '../actions/types';

const initialState = {
  items: [],
  item: {}
}

export default function(state = initialState, action) {
  switch(action.type) {  // we are evaluating the FETCH_POSTS / NEW_POST here
    case FETCH_POSTS:
      return {
        ...state,
        items: action.payload  // remember we used payload for our posts data
      }
    case NEW_POST:
      return {
        ...state,
        item: action.payload
      }
    default:
      return state;
  }
}
