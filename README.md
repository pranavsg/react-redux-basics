# React-Redux-Basics

Learning how to implement Redux using a simple React project

```
npm install -g create-react-app
npx create-react-app reduxexample
```

Checkout the basic **Posts & PostForm** component inside **src/components/** which will
be used to communicate with each other using the *Redux*.

We are using the free [JsonPlaceholder API](https://jsonplaceholder.typicode.com/) for
fetching and creating sample posts data.

```
npm i -s redux react-redux redux-thunk
```

`react-redux` binds react and redux together
`redux-thunk` is the middleware for redux
and `redux` itself needs to be installed.
